/***************************************************
 ** @Desc : This file for 代付实现方法
 ** @Time : 2019.04.16 17:08 
 ** @Author : Joker
 ** @File : pay_impl
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.16 17:08
 ** @Software: GoLand
****************************************************/
package implement

import (
	"recharge/models"
	"recharge/utils"
	"regexp"
	"strconv"
	"strings"
)

type RechargePayImpl struct{}

var userExpandMdl = models.UserInfoExpansion{}

// 验证代付信息
func (*RechargePayImpl) VerificationPayInfo(accountNo, accountName, money string) (string, bool) {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	if accountNo == "" || accountName == "" || money == "" {
		return "银行卡或户名不能为空!", false
	}
	if !matched1 {
		return "请输入正确的充值金额哦!", false
	}

	float, _ := strconv.ParseFloat(money, 10)
	if float > utils.SingleMaxForPay {
		return "代发金额超限!", false
	}
	return "", true
}

// 验证短信或ip
func (*RechargePayImpl) VerificationSmgOrIP(userId int, code interface{}, ip, mobileCode string) (string, bool) {
	ux := userExpandMdl.SelectOneUserInfoExpansion(userId)
	if ux.Ips == "" {
		if code == nil {
			return "请发送手机验证码！", false
		}

		if strings.Compare(code.(string), mobileCode) != 0 {
			return "短信验证码输入错误！", false
		}

		return "", true
	} else {
		contains := strings.Contains(ux.Ips, ip)
		if contains {
			return "", true
		}
		return "出款IP不在白名单中！赶紧将IP发给管理绑定！", false
	}
}

// 验证批量代付信息
func (*RechargePayImpl) VerificationBulkPayInfo(money, allRecords, mobileNo string) (string, bool) {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	matched2, _ := regexp.MatchString(`^[1-9]\d*$`, allRecords)
	matched3, _ := regexp.MatchString(`^[1]([3-9])[0-9]{9}$`, mobileNo)

	if !matched1 {
		return "请输入正确的金额哦", false
	}
	if !matched2 {
		return "请输入正确的笔数哦!", false
	}
	if !matched3 {
		return "请输入正确的手机号!", false
	}
	return "", true
}

// 验证批量充值信息
func (*RechargePayImpl) VerificationBulkRechargeInfo(money, allRecords string) (string, bool) {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	matched2, _ := regexp.MatchString(`^[1-9]\d*$`, allRecords)

	if !matched1 {
		return "请输入正确的金额哦", false
	}
	if !matched2 {
		return "请输入正确的笔数哦!", false
	}
	return "", true
}

// 验证代付金额
func (*RechargePayImpl) VerificationBulkPayMoney(money string) bool {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	if !matched1 {
		return false
	}
	float, _ := strconv.ParseFloat(money, 10)
	if float > utils.SingleMaxForPay {
		return false
	}
	return true
}

// 验证充值金额
func (*RechargePayImpl) VerificationBulkRechargeMoney(money string) bool {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	if !matched1 {
		return false
	}
	float, _ := strconv.ParseFloat(money, 10)
	if float < utils.SingleMinForRecharge {
		return false
	}
	return true
}