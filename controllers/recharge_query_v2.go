/***************************************************
 ** @Desc : This file for 充值查询
 ** @Time : 2019.04.11 9:42
 ** @Author : Joker
 ** @File : recharge_query
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.11 9:42
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"recharge/models"
	"recharge/sys"
	"recharge/utils"
	"recharge/utils/interface_config"
	"strings"
)

type RechargeQueryV2 struct {
	KeepSession
}

// 充值单笔查询
// @router /merchant/recharge_query_v2/?:params [get]
func (the *RechargeQueryV2) RechargeQuery() {
	queryId := the.GetString(":params")

	record := rechargeMdl.SelectOneRechargeRecord(queryId)
	merchant := merchantMdl.SelectOneMerchantByNo(record.MerchantNo)

	var msg = utils.SUCCESS_STRING
	var flag = utils.FAILED_FLAG

	//只查询处理中的订单
	if strings.Compare(utils.I, record.Status) == 0 {
		// 多通道充值
		switch merchant.ChannelType {
		case utils.XF:
			msg, flag = xfRechargeQueryV2(record, merchant, msg, flag)
		}
	} else {
		msg = "只查询处理中的订单！"
	}

	the.Data["json"] = globalMethod.JsonFormat(flag, "", msg, "")
	the.ServeJSON()
	the.StopRun()
}

// 先锋通道查询
func xfRechargeQueryV2(record models.RechargeRecord, merchant models.Merchant, msg string, flag int) (string, int) {
	//先锋
	encode := encrypt.EncodeMd5([]byte(globalMethod.RandomString(32)))
	result, err := xfRechargeQueryReqV2(record, encode, merchant.SecretKey)
	sys.LogInfo("先锋返回查询信息:", string(result))
	if err != nil {
		return err.Error(), flag
	}

	resp := models.XFRechargeResponseBody{}
	err = json.Unmarshal(result, &resp)
	if err != nil {
		sys.LogError("response data format is error:", err)
		return "先锋充值查询响应数据格式错误", flag
	}

	if strings.Compare("00000", resp.ResCode) != 0 {
		record.EditTime = globalMethod.GetNowTime()
		record.Remark = resp.ResMessage
		rechargeMdl.UpdateRechargeRecord(record)
		return "先锋充值查询错误：" + resp.ResMessage, flag
	}

	record.EditTime = globalMethod.GetNowTime()
	record.Status = resp.Status
	record.Remark = resp.ResMessage

	switch resp.Status {
	case utils.S:
		// 加款
		flag = userMdl.SelectOneUserAddition(record.UserId, record.ReAmount, record)
		if flag > 0 {
			sys.LogInfo("先锋充值订单:", record.ReOrderId, "加款成功")
		} else {
			sys.LogInfo("先锋充值订单:", record.ReOrderId, "加款失败")
		}
	case utils.F:
		msg = resp.ResMessage
		flag = rechargeMdl.UpdateRechargeRecord(record)
	case utils.I:
		msg = "订单处理中，查询信息：" + resp.ResMessage
	}

	return msg, flag
}

// 先锋道通充值查询
func xfRechargeQueryReqV2(record models.RechargeRecord, b, key string) ([]byte, error) {
	// 请求参数
	reqParams := url.Values{}
	reqParams.Add("service", "REQ_RECHARGE_QUERY")
	reqParams.Add("version", interface_config.VERSION_V2)
	reqParams.Add("merchantId", record.MerchantNo)
	blockKey, err := merchantImpl.XFHandleBlockKey(b, key)
	if err != nil {
		s := "先锋充值aes秘钥加密失败"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}
	reqParams.Add("tm", blockKey)

	// 需要的加密参数
	dataParams := models.XFRechargeQueryData{}
	dataParams.MerchantNo = record.ReOrderId

	// 加密参数
	dataString, _ := json.Marshal(&dataParams)
	data, err := AES.AesEncryptV2(string(dataString), b)
	if err != nil {
		return nil, err
	}

	reqParams.Add("data", data)

	// 生成签名
	respParams := map[string]string{}
	respParams["service"] = "REQ_RECHARGE_QUERY"
	respParams["version"] = interface_config.VERSION_V2
	respParams["merchantId"] = record.MerchantNo
	respParams["data"] = data
	respParams["tm"] = blockKey
	params := globalMethod.ToStringByMap(respParams)

	signBytes, err := merchantImpl.XFGenerateSignV2(params, interface_config.PRIVATE_KEY)
	if err != nil {
		s := "先锋充值查询生成签名失败"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}
	reqParams.Add("sign", string(signBytes))

	// 发送请求
	resp, err := http.PostForm(interface_config.XF_RECHANGE_URL, reqParams)
	if err != nil {
		s := "先锋充值查询请求失败"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}

	// 处理响应
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		s := "先锋充值查询响应为空"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}
	defer resp.Body.Close()

	respData := models.RespData{}
	err = json.Unmarshal(body, &respData)
	if err != nil {
		s := "先锋充值查询响应数据格式错误"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}

	if respData.Data == "" {
		sys.LogInfo("先锋充值查询响应：", string(body))
		return nil, errors.New("先锋充值查询响应data为空！")
	}

	s, err := merchantImpl.DecryptRespDataByPrivateKey(respData.Tm)
	bytes, err := AES.AesDecryptV2([]byte(respData.Data), s)
	if err != nil {
		s := "先锋充值查询响应解密错误"
		sys.LogTrace(s, err)
		return nil, errors.New(s)
	}

	return bytes, err
}
